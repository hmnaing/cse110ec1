package edu.ucsd.cse110.library;

public class FactoryPubAdmin {
	private Member member;
	private Publication publication;
	public FactoryPubAdmin(Member m, Publication p){
		member = m;
		publication = p;
	}
	
	public PubAdmin getPubAdmin(){
		return new PubAdmin(member, publication);
	}
}
