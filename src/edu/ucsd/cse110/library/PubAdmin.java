package edu.ucsd.cse110.library;

import java.time.LocalDate;
//enterprise component with rule objects
public class PubAdmin {
	private Member member;
	private Publication publication;
	public PubAdmin (Member m, Publication p) {
		member = m;
		publication = p;
	}

	public CheckoutMediator cMed = new CheckoutMediator();
	public FeeMediator fMed = new FeeMediator();

	public void checkoutPublication() {
		cMed.checkoutPub(member, publication);
	}

	public void returnPublication() {
		cMed.returnPub(publication);
	}

	public double getFee() {
		return fMed.getFee(member);
	}

	public boolean hasFee(){
		return fMed.hasFee(member);
	}


}
