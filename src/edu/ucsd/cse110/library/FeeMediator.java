package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class FeeMediator {

	public FeeMediator() {}

	public double getFee(Member m) {
		return m.getDueFees();
	}

	public boolean hasFee(Member m){
		if (m.getDueFees() > 0.0) {
			return true;
		}
		else {
			return false;
		}
	}
}
